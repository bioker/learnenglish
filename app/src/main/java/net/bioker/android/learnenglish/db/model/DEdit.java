package net.bioker.android.learnenglish.db.model;

public class DEdit {

    public static final String EDIT = "EDIT";
    public static final String EDIT_QUESTION = "QUESTION";
    public static final String EDIT_DESCRIPTION = "DESCRIPTION";
    public static final String EDIT_EXERCISE = "EXERCISE";

    public static final String ANSWER = "SIMPLE_ANSWER";
    public static final String ANSWER_TEXT = "ANSWER";
    public static final String ANSWER_DESCRIPTION = "DESCRIPTION";
    public static final String ANSWER_TEST = "QUESTION";


    public static final String TABLE_CREATE_SIMPLE = "CREATE TABLE "+ EDIT
            +" (_ID INTEGER PRIMARY KEY, "
            + EDIT_QUESTION +" TEXT NOT NULL, "
            + EDIT_DESCRIPTION +" TEXT NULL, "
            + EDIT_EXERCISE +" INTEGER, FOREIGN KEY("+ DMain.EXERCISE +") REFERENCES THEME(_ID));";

    public static final String TABLE_CREATE_ANSWERS = "CREATE TABLE "+ANSWER
            +" (_ID INTEGER PRIMARY KEY, "
            +ANSWER_TEXT+" TEXT NOT NULL, "
            +ANSWER_DESCRIPTION+" TEXT NULL, "
            + ANSWER_TEST +" INTEGER, FOREIGN KEY("+ ANSWER_TEST +") REFERENCES QUESTION(_ID));";

}
