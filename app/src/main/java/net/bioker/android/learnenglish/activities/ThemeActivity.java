package net.bioker.android.learnenglish.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.db.control.DMainController;
import net.bioker.android.learnenglish.model.Theme;

import java.util.ArrayList;

public class ThemeActivity extends AppCompatActivity {
    private String[] menuTitles;
    private ListView mDrawerListView;

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        Context context;
        DrawerItemClickListener(Context context){
            super();
            this.context = context;
        }
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position){
                case 0:{
                    Intent intent = new Intent(context, ThemeActivity.class);
                    context.startActivity(intent);
                    break;
                }
                case 1:{
                    Intent intent = new Intent(context, LoaderActivity.class);
                    context.startActivity(intent);
                    break;
                }
                case 2: {
                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                    break;
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);

        menuTitles=getResources().getStringArray(R.array.menu);
        mDrawerListView = (ListView) findViewById(R.id.left_drawer);
        mDrawerListView.setAdapter(new ArrayAdapter<String>(this,R.layout.drawer_list_item,menuTitles));
        mDrawerListView.setOnItemClickListener(new DrawerItemClickListener(this));

        ListView themesList = (ListView) findViewById(R.id.themesList);
        fillThemesList(themesList);
    }

    private static void fillThemesList(ListView view) {
        final ArrayList<Theme> themeArrayList = DMainController.getAllThemes();
        if(themeArrayList != null){
            if (themeArrayList.size() > 0) {
                ArrayAdapter<Theme> themeArrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, themeArrayList);
                view.setAdapter(themeArrayAdapter);
                view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        long themeId = ((Theme) adapterView.getItemAtPosition(i)).getId();
                        Log.d("JSON","themeId "+themeId);
                        Toast.makeText(view.getContext(), "select " + themeId + " theme", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(view.getContext(), LessonActivity.class);
                        intent.putExtra("theme", themeId);
                        view.getContext().startActivity(intent);
                    }
                });
            }
            else {
                throw new NullPointerException("themes not found");
            }
        }
        else{

        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        finishAffinity();
    }


}