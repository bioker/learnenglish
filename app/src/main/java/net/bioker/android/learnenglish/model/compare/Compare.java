package net.bioker.android.learnenglish.model.compare;

import net.bioker.android.learnenglish.db.model.DCompare;
import net.bioker.android.learnenglish.model.Exercise;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Model of Compare.
 */
public class Compare extends Exercise implements Parcelable {


    public static final Creator<Compare> CREATOR = new Creator<Compare>() {
        @Override
        public Compare createFromParcel(Parcel in) {
            return new Compare(in);
        }

        @Override
        public Compare[] newArray(int size) {
            return new Compare[size];
        }
    };

    public ArrayList<Pair> pairs;
    private long id;
    private String description;
    private long exercise;

    public Compare(long id, ArrayList<Pair> pairs, long exercise, String description) {
        super(Type.Compare);
        this.id = id;
        this.pairs = pairs;
        this.exercise = exercise;
        this.description = description;
    }

    public Compare(String JSONString) throws JSONException {
        super(Type.Compare);
        JSONArray JSON = new JSONArray(JSONString);

        JSONArray JSONPairs = JSON.getJSONObject(0).getJSONArray("PAIRS");
        Log.d("JSON","Pairs-"+JSONPairs.toString());
        ArrayList<Pair> pairs = new ArrayList<>();
        for(int i=0; i<JSONPairs.length(); i++){
            JSONObject JSONPair = JSONPairs.getJSONObject(i);
            pairs.add(new Pair(
                    JSONPair.getLong("_ID"),
                    JSONPair.getString(DCompare.PAIR_ANSWER1),
                    JSONPair.getString(DCompare.PAIR_ANSWER2),
                    JSONPair.getString(DCompare.PAIR_DESCRIPTION)
            ));
        }

                this.id =JSON.getJSONObject(0).getInt("_ID");
                this.pairs = pairs;
                this.exercise = JSON.getJSONObject(0).getLong(DCompare.COMPARE_EXERCISE);
                this.description = JSON.getJSONObject(0).getString(DCompare.COMPARE_DESCRIPTION);

    }

    protected Compare(Parcel in) {
        super(Type.Compare);
        pairs = in.createTypedArrayList(Pair.CREATOR);
        id = in.readLong();
        description = in.readString();
        exercise = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(pairs);
        dest.writeLong(id);
        dest.writeString(description);
        dest.writeLong(exercise);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public long getId() {
        return id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public long getExercise() {
        return exercise;
    }

    public ArrayList<Pair> getPairs() {
        return pairs;
    }

    @Override
    public String toString() {
        return "Compare{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", exercise='" + exercise + '\'' +
                "}\n" + pairs.toString();
    }

    @Override
    public Type getType() {
        return Type.Compare;
    }

}

