package net.bioker.android.learnenglish.model.test;

import android.os.Parcel;
import android.os.Parcelable;

public class Answer implements Parcelable {

    public static final Creator<Answer> CREATOR = new Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel in) {
            return new Answer(in);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };

    private long id;
    private long question;
    private String value;
    private String description;
    private boolean isRight;
    private long numberOfUse;

    public Answer(long id, long question, String value, String description, boolean isRight, long numberOfUse) {
        this.id = id;
        this.question = question;
        this.value = value;
        this.description = description;
        this.isRight = isRight;
        this.numberOfUse = numberOfUse;
    }

    protected Answer(Parcel in) {
        id = in.readLong();
        question = in.readLong();
        value = in.readString();
        description = in.readString();
        isRight = in.readByte() != 0;
        numberOfUse = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(question);
        dest.writeString(value);
        dest.writeString(description);
        dest.writeByte((byte) (isRight ? 1 : 0));
        dest.writeLong(numberOfUse);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getId() {
        return id;
    }

    public long getQuestion() {
        return question;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRight() {
        return isRight;
    }

    public long getNumberOfUse() {
        return numberOfUse;
    }

    @Override
    public String toString() {
        return value;
    }
}
