package net.bioker.android.learnenglish.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.adapters.ProgressInfoAdapter;
import net.bioker.android.learnenglish.model.profile.ProgressInfoItem;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    private ListView progressInfoItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        progressInfoItemsList = (ListView) findViewById(R.id.progressInfoItemsList);
        Log.d("Profile","list taken");
        ArrayList<ProgressInfoItem> items = ProgressInfoItem.getSummaryProfileInfo(this);
        ProgressInfoAdapter adapter = new ProgressInfoAdapter(this,items);
        Log.d("Profile","adapter taken");
        progressInfoItemsList.setAdapter(adapter);
        Log.d("Profile","adapter was set");
    }

}
