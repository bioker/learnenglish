package net.bioker.android.learnenglish.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.bioker.android.learnenglish.db.model.DCompare;
import net.bioker.android.learnenglish.db.model.DMain;
import net.bioker.android.learnenglish.db.model.DEdit;
import net.bioker.android.learnenglish.db.model.DTest;
import net.bioker.android.learnenglish.db.model.DVersion;

public class DBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "english_db";

    public static final int DATABASE_VERSION = 2;//fix it

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DVersion.TABLE_CREATE_VERSION);
        db.execSQL(DMain.TABLE_CREATE_THEMES);
        db.execSQL(DMain.TABLE_CREATE_LESSON);
        db.execSQL(DMain.TABLE_CREATE_EXERCISE);

        db.execSQL(DTest.TABLE_CREATE_TEST);
        db.execSQL(DTest.TABLE_CREATE_ANSWERS);

        db.execSQL(DCompare.TABLE_CREATE_SOPS);
        db.execSQL(DCompare.TABLE_CREATE_PAIR);

        db.execSQL(DEdit.TABLE_CREATE_SIMPLE);
        db.execSQL(DEdit.TABLE_CREATE_ANSWERS);

        Log.d("dboh", "database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DEdit.EDIT);
        db.execSQL("DROP TABLE IF EXISTS " + DEdit.ANSWER);

        db.execSQL("DROP TABLE IF EXISTS " + DCompare.PAIR);
        db.execSQL("DROP TABLE IF EXISTS " + DCompare.COMPARE);

        db.execSQL("DROP TABLE IF EXISTS " + DTest.ANSWER);
        db.execSQL("DROP TABLE IF EXISTS " + DTest.TEST);

        db.execSQL("DROP TABLE IF EXISTS " + DMain.EXERCISE);
        db.execSQL("DROP TABLE IF EXISTS " + DMain.LESSON);
        db.execSQL("DROP TABLE IF EXISTS " + DMain.THEME);
        db.execSQL("DROP TABLE IF EXISTS " + DVersion.VERSION);

        onCreate(db);
    }
}