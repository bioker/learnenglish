package net.bioker.android.learnenglish.db.model;

public class DMain {

    public static final String THEME = "THEME";
    public static final String THEME_NAME = "NAME";
    public static final String THEME_DESCRIPTION = "DESCRIPTION";


    public static final String LESSON = "LESSON";
    public static final String LESSON_NAME = "NAME";
    public static final String LESSON_THEME = "THEME";
    public static final String LESSON_DESCRIPTION = "DESCRIPTION";


    public static final String EXERCISE = "EXERCISE";
    /**
     * Type of tasks:
     * 1 - Test
     * 2 - Compare
     * 3 - Edit
     */
    public static final int TYPE_TEST = 1;
    public static final int TYPE_COMPARE = 2;
    public static final int TYPE_EDIT = 3;

    public static final String EXERCISE_TYPE = "TYPE";
    public static final String EXERCISE_LESSON = "LESSON";
    public static final String EXERCISE_IDINTABLE = "IDINTABLE";
    public static final String EXERCISE_NUMOFUSE = "NUMOFUSE";
    public static final String EXERCISE_ERRORS = "ERRORS";

    public static final String TABLE_CREATE_THEMES = "CREATE TABLE "+THEME
            +" (_ID INTEGER PRIMARY KEY, "
            +THEME_NAME+" TEXT NOT NULL, "
            +THEME_DESCRIPTION+" TEXT NULL); ";

    public static final String TABLE_CREATE_LESSON = "CREATE TABLE "+ LESSON
            +" (_ID INTEGER PRIMARY KEY, "
            + LESSON_THEME +" INTEGER NOT NULL, "
            + LESSON_NAME +" TEXT NULL, "
            + LESSON_DESCRIPTION +" TEXT NULL, "
            +"FOREIGN KEY("+ LESSON_THEME +") REFERENCES "+THEME+"(_ID)); ";

    public static final String TABLE_CREATE_EXERCISE = "CREATE TABLE "+ EXERCISE
            +" (_ID INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EXERCISE_LESSON +" INTEGER NOT NULL,"
            + EXERCISE_TYPE +" INTEGER NOT NULL, "
            + EXERCISE_NUMOFUSE +" INTEGER NOT NULL DEFAULT 0, "
            + EXERCISE_ERRORS +" INTEGER NOT NULL DEFAULT -1, "
            + EXERCISE_IDINTABLE +" INTEGER NOT NULL, "
            +"FOREIGN KEY("+ EXERCISE_LESSON +") REFERENCES "+ LESSON +"(_ID)); ";
}
