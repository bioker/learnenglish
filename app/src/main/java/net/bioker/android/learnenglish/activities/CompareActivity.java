package net.bioker.android.learnenglish.activities;

import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.adapters.CompareAnswersAdapter;
import net.bioker.android.learnenglish.model.compare.Compare;
import net.bioker.android.learnenglish.model.compare.Pair;
import net.bioker.android.learnenglish.model.draganddrop.CompareAnswerView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;

public class CompareActivity extends AppCompatActivity {

    private ListView leftList;
    private ListView rightList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare);
        Intent intent = getIntent();
        Compare compare = null;
        try {
            compare = new Compare(intent.getStringExtra("compare"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (compare != null) {
            Log.i("TAG", "compare: \n" + compare.toString());

            ArrayList<Pair> pairs1 = new ArrayList<>();
            pairs1.addAll(compare.getPairs());
            Collections.shuffle(compare.getPairs());
            ArrayList<Pair> pairs2 = new ArrayList<>();
            pairs2.addAll(compare.getPairs());

            CompareAnswersAdapter compareAnswersAdapter1 = new CompareAnswersAdapter(this, pairs1, true);
            CompareAnswersAdapter compareAnswersAdapter2 = new CompareAnswersAdapter(this, pairs2, false);

            leftList = (ListView) findViewById(R.id.variantsColumnLeft);
            leftList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            leftList.setAdapter(compareAnswersAdapter1);
            leftList.setOnItemLongClickListener(myOnItemLongClickListener);

            rightList = (ListView) findViewById(R.id.variantsColumnRight);
            rightList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            rightList.setAdapter(compareAnswersAdapter2);
            rightList.setOnItemLongClickListener(myOnItemLongClickListener);
        } else {
            Toast.makeText(this, "Exercise not found", Toast.LENGTH_SHORT).show();
            try {
                LessonActivity.launchExercise(getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    AdapterView.OnItemLongClickListener myOnItemLongClickListener = new AdapterView.OnItemLongClickListener() {

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view,
                                       int position, long id) {
            Pair selectedItem = (Pair) (parent.getItemAtPosition(position));

            CompareAnswerView passObj = new CompareAnswerView(selectedItem.getId(), view);

            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, passObj, 0);

            return true;
        }

    };

}
