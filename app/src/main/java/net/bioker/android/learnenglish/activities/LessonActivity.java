package net.bioker.android.learnenglish.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.db.control.DMainController;
import net.bioker.android.learnenglish.model.Lesson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class LessonActivity extends AppCompatActivity {

    public static ArrayList<Integer> lessonIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
        ListView lessonsList = (ListView) findViewById(R.id.lessonsList);
        try {
            fillLessonsList(lessonsList, getIntent().getLongExtra("theme", -1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static void fillLessonsList(ListView view, long themeId) throws JSONException{
        if (themeId > 0) {
            Log.d("JSON", String.valueOf(themeId));
            final ArrayList<Lesson> lessonsArrayList = DMainController.getAllLessons(themeId);
            if(lessonsArrayList.isEmpty()||lessonsArrayList==null)Log.d("JSON",":(");else Log.d("JSON",":)");
            if (lessonsArrayList.size() > 0) {
                final ArrayAdapter<Lesson> lessonsArrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, lessonsArrayList);
                view.setAdapter(lessonsArrayAdapter);
                view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        long lessonId = ((Lesson) adapterView.getItemAtPosition(i)).getId();
                        Toast.makeText(view.getContext(), DMainController.getNameLesson(lessonId), Toast.LENGTH_SHORT).show();
                        lessonIds = DMainController.getAllExercise(lessonId);
                        try {
                            launchExercise(view.getContext());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                throw new NullPointerException("lessons not found");
            }
        } else {
            throw new NullPointerException("theme not set");
        }
    }

    public static void launchExercise(Context context) throws JSONException {
        if (lessonIds != null && lessonIds.size() > 0) {
            Integer exerciseId = lessonIds.get(0);
            lessonIds.remove(0);
            Log.d("JSON", "launch exercise id:"+exerciseId);
            JSONArray exercise = DMainController.makeExercise(exerciseId);
            Log.d("JSON", "launch exercise:"+exercise.toString());
            switch (exercise.getJSONObject(exercise.length()-1).getString("TYPE")){
                case "compare":{
                    Intent intent = new Intent(context,CompareActivity.class);
                    intent.putExtra("compare", exercise.toString());
                    Log.d("JSON", "launch compare: \n" +  exercise.toString());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } break;
                case "test":{
                    Intent intent = new Intent(context, TestActivity.class);
                    intent.putExtra("test",  exercise.toString());
                    Log.d("JSON", "launch test: \n" +  exercise.toString());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } break;
               case "simple":{
                    Intent intent = new Intent(context, EditActivity.class);
                    intent.putExtra("simple",  exercise.toString());
                    Log.d("JSON", "launch simple: \n" +  exercise.toString());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } break;
                default:{
                    Toast.makeText(context, "exercise type not support", Toast.LENGTH_SHORT).show();
                } break;
            }
        } else {
            Toast.makeText(context, "don't have a lessonIds", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(context, ThemeActivity.class);
            intent.putExtra("isFinished", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

}
