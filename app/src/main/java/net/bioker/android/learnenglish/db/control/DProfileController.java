package net.bioker.android.learnenglish.db.control;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.learnenglish.db.model.DMain;

public class DProfileController {

    public static final String COUNT_FIELD_NAME = "COUNT";

    public static float progressCoefForTheme(SQLiteDatabase db, long themeId) {
        int all = getCountField(getAllExercisesCountQueryByTheme(themeId), db);
        int passed = getCountField(getPassedExercisesCountQueryByTheme(themeId), db);
        Log.d("MYTAG", "theme " + themeId + " progress all/passed - " + all + "/" + passed);
        return (float) passed / (float) all;
    }

    public static float progressCoefForLesson(SQLiteDatabase db, long lessonId) {
        int all = getCountField(getAllExercisesCountQueryByLesson(lessonId), db);
        int passed = getCountField(getPassedExercisesCountQueryByLesson(lessonId), db);
        Log.d("MYTAG", "lesson " + lessonId + " progress all/passed - " + all + "/" + passed);
        return (float) passed / (float) all;
    }

    public static float errorsCoefForTheme(SQLiteDatabase db, long themeId) {
        int passed = getCountField(getPassedExercisesCountQueryByTheme(themeId), db);
        int bad = getCountField(getBadExercisesCountQueryByTheme(themeId), db);
        Log.d("MYTAG", "theme " + themeId + " errors bad/passed - " + bad + "/" + passed);
        return (float) bad / (float) passed;
    }

    public static float errorsCoefForLesson(SQLiteDatabase db, long lessonId) {
        int passed = getCountField(getPassedExercisesCountQueryByLesson(lessonId), db);
        int bad = getCountField(getBadExercisesCountQueryByLesson(lessonId), db);
        Log.d("MYTAG", "lesson " + lessonId + " errors bad/passed - " + bad + "/" + passed);
        return (float) bad / (float) passed;
    }

    private static int getCountField(String sql, SQLiteDatabase db) {
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            return cursor.getInt(cursor.getColumnIndex(COUNT_FIELD_NAME));
        }
        throw new NullPointerException("records not found");
    }

    private static String getPassedExercisesCountQueryByTheme(long themeId) {
        return
                "SELECT COUNT(*) AS " + COUNT_FIELD_NAME + " FROM " + DMain.EXERCISE +
                        " WHERE " + DMain.EXERCISE_LESSON + " = " +
                        "(SELECT _ID FROM " + DMain.LESSON + " WHERE " +
                        "_ID = " + String.valueOf(themeId) + ") AND " + DMain.EXERCISE_NUMOFUSE +
                        " > 0";
    }

    private static String getPassedExercisesCountQueryByLesson(long lessonId) {
        return
                "SELECT COUNT(*) AS " + COUNT_FIELD_NAME + " FROM " + DMain.EXERCISE +
                        " WHERE " + DMain.EXERCISE_LESSON + " = " + String.valueOf(lessonId) +
                        " AND " + DMain.EXERCISE_NUMOFUSE +
                        " > 0";
    }

    private static String getAllExercisesCountQueryByTheme(long themeId) {
        return
                "SELECT COUNT(*) AS " + COUNT_FIELD_NAME + " FROM " + DMain.EXERCISE +
                        " WHERE " + DMain.EXERCISE_LESSON + " = " +
                        "(SELECT _ID FROM " + DMain.LESSON + " WHERE " +
                        "_ID = " + String.valueOf(themeId) + ")";
    }

    private static String getAllExercisesCountQueryByLesson(long lessonId) {
        return
                "SELECT COUNT(*) AS " + COUNT_FIELD_NAME + " FROM " + DMain.EXERCISE +
                        " WHERE " + DMain.EXERCISE_LESSON + " = " + String.valueOf(lessonId);
    }

    private static String getBadExercisesCountQueryByTheme(long themeId) {
        return
                "SELECT COUNT(*) AS " + COUNT_FIELD_NAME + " FROM " + DMain.EXERCISE +
                        " WHERE " + DMain.EXERCISE_LESSON + " = " +
                        "(SELECT _ID FROM " + DMain.LESSON + " WHERE " +
                        "_ID = " + String.valueOf(themeId) + ") AND " + DMain.EXERCISE_ERRORS +
                        " > 0";
    }

    private static String getBadExercisesCountQueryByLesson(long lessonId) {
        return
                "SELECT COUNT(*) AS " + COUNT_FIELD_NAME + " FROM " + DMain.EXERCISE +
                        " WHERE " + DMain.EXERCISE_LESSON + " = " + String.valueOf(lessonId) +
                        " AND " + DMain.EXERCISE_ERRORS +
                        " > 0";
    }

    static String getUseExerciseQuery(long exerciseId){
        return
                "UPDATE " + DMain.EXERCISE +
                " SET " + DMain.EXERCISE_NUMOFUSE + "=" + DMain.EXERCISE_NUMOFUSE + " + 1" +
                        " WHERE _ID=" + exerciseId;
    }

    static String getErrorExerciseQuery(long exerciseId){
        return
                "UPDATE " + DMain.EXERCISE +
                        " SET " + DMain.EXERCISE_ERRORS + "=" + DMain.EXERCISE_ERRORS + " + 1" +
                        " WHERE _ID=" + exerciseId;
    }

}
