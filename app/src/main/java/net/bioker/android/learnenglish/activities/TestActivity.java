package net.bioker.android.learnenglish.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.fragments.NextExercise;
import net.bioker.android.learnenglish.model.test.Answer;
import net.bioker.android.learnenglish.model.test.Test;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;

public class TestActivity extends AppCompatActivity {
    NextExercise nextFrag = new NextExercise(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Intent intent = getIntent();
        Test test = null;
        try {test = new Test(intent.getStringExtra("test"));} catch (JSONException e) {e.printStackTrace();}
        if (test != null) {
            reloadUI(test);
        } else {
            Toast.makeText(getApplicationContext(), "test not found", Toast.LENGTH_SHORT).show();
            try {
                LessonActivity.launchExercise(getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void reloadUI(Test test) {
        TextView testText = (TextView) findViewById(R.id.testText);
        testText.setText(test.getQuestion());
        ListView testAnswersList = (ListView) findViewById(R.id.testAnswersList);
        Collections.shuffle(test.getAnswers());
        reloadAnswersList(testAnswersList, test.getAnswers());
    }

    private void reloadAnswersList(ListView view, ArrayList<Answer> answers) {
        Collections.shuffle(answers);
        ArrayAdapter<Answer> answerArrayAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, answers);
        view.setAdapter(answerArrayAdapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean answer = ((Answer) parent.getItemAtPosition(position)).isRight();
                String errDesc = ((Answer) parent.getItemAtPosition(position)).getDescription();

                    nextFrag.wrongAns(errDesc,answer);
                    nextFrag.show(getFragmentManager(), null);

            }
        });
    }

}
