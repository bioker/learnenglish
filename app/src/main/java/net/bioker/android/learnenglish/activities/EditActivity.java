package net.bioker.android.learnenglish.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.model.Edit.Edit;
import net.bioker.android.learnenglish.model.test.Answer;
import net.bioker.android.learnenglish.model.test.Test;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;

public class EditActivity extends AppCompatActivity {
    Edit edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Intent intent = getIntent();
        try {edit = new Edit(intent.getStringExtra("simple"));} catch (JSONException e) {e.printStackTrace();}
        if (edit != null) {
            reloadUI();
        } else {
            Toast.makeText(getApplicationContext(), "Edit not found", Toast.LENGTH_SHORT).show();
            try {
                LessonActivity.launchExercise(getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void reloadUI() {
        TextView editText = (TextView) findViewById(R.id.questionText);
        editText.setText(this.edit.getQuestion());
        Collections.shuffle(this.edit.getAnswers());
    }

    private boolean check() {
        EditText answer = (EditText) findViewById(R.id.answerEdit);
        for(int i =0;edit.getAnswers().size()>i;i++){
            if(edit.getAnswers().get(i).first.toUpperCase().trim().equals(answer.getText().toString().toUpperCase().trim())){
                Log.d("JSON","check++-"+answer.getText().toString()+"="+edit.getAnswers().get(i).first.toUpperCase());
                return true;
            };
        }
        return false;
    }


    public void checkEdit(View view) {
            if(check()){
                try {
                    LessonActivity.launchExercise(this);
                    Toast.makeText(this, "RIGHT", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    LessonActivity.launchExercise(this);
                    Toast.makeText(this, "WRONG", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
}
