package net.bioker.android.learnenglish.model.test;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import net.bioker.android.learnenglish.db.model.DTest;
import net.bioker.android.learnenglish.model.Exercise;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Model of Test.
 */
public class Test extends Exercise implements Parcelable {

    public static final Creator<Test> CREATOR = new Creator<Test>() {
        @Override
        public Test createFromParcel(Parcel in) {
            return new Test(in);
        }

        @Override
        public Test[] newArray(int size) {
            return new Test[size];
        }
    };

    public ArrayList<Answer> answers;
    private long id;
    private String question;
    private String description;
    private long numberOfUse;
    private long exercise;
    private long difficulty;

    public Test(long id, String question, long difficulty, String description, long numberOfUse, long exercise, ArrayList<Answer> answers) {
        super(Type.Test);
        this.id = id;
        this.question = question;
        this.description = description;
        this.numberOfUse = numberOfUse;
        this.exercise = exercise;
        this.difficulty = difficulty;
        this.answers = answers;
    }

    protected Test(Parcel in) {
        super(Type.Test);
        id = in.readLong();
        question = in.readString();
        description = in.readString();
        numberOfUse = in.readLong();
        exercise = in.readLong();
        difficulty = in.readLong();
        answers = in.createTypedArrayList(Answer.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(question);
        dest.writeString(description);
        dest.writeLong(numberOfUse);
        dest.writeLong(exercise);
        dest.writeLong(difficulty);
        dest.writeTypedList(answers);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    @Override
    public String getDescription() {
        return description;
    }


    public Test(String JSONString) throws JSONException {
        super(Type.Test);
        JSONArray JSON = new JSONArray(JSONString);
//PIZDARIQUE
        JSONArray JSONra = JSON.getJSONArray(1).getJSONArray(0);
        JSONArray JSONwa = JSON.getJSONArray(1).getJSONArray(1);
        Log.d("JSON","TEST-"+JSON.toString());
        Log.d("JSON","RA-"+JSONra.toString());
        Log.d("JSON","WA-"+JSONwa.toString());
        ArrayList<Answer> answers = new ArrayList<>();
        for(int i=0; i<JSONra.length(); i++){
            JSONObject JSONPair = JSONra.getJSONObject(i);
            answers.add(new Answer(
                    JSONPair.getLong("_ID"),
                    JSONPair.getLong(DTest.ANSWER_TEST),
                    JSONPair.getString(DTest.ANSWER_TEXT),
                    JSONPair.getString(DTest.ANSWER_DESCRIPTION),
                    JSONPair.getBoolean(DTest.ANSWER_ISRIGHT),
                    JSONPair.getLong(DTest.ANSWER_USE)
            ));
        }
        for(int i=0; i<JSONwa.length(); i++){
            JSONObject JSONPair = JSONwa.getJSONObject(i);
            answers.add(new Answer(
                    JSONPair.getLong("_ID"),
                    JSONPair.getLong(DTest.ANSWER_TEST),
                    JSONPair.getString(DTest.ANSWER_TEXT),
                    JSONPair.getString(DTest.ANSWER_DESCRIPTION),
                    JSONPair.getBoolean(DTest.ANSWER_ISRIGHT),
                    JSONPair.getLong(DTest.ANSWER_USE)
            ));
        }

        this.id = JSON.getJSONObject(0).getInt("_ID");
        this.question = JSON.getJSONObject(0).getString(DTest.TEST_QUESTION);
        this.description = JSON.getJSONObject(0).getString(DTest.TEST_DESCRIPTION);
        this.numberOfUse = JSON.getJSONObject(0).getInt(DTest.TEST_USE);
        this.exercise = JSON.getJSONObject(0).getInt(DTest.TEST_EXERCISE);
        this.difficulty = JSON.getJSONObject(0).getInt(DTest.TEST_DIFFICULTY);
        this.answers = answers;
        Log.d("JSON",answers.toString());

    }

    public long getNumberOfUse() {
        return numberOfUse;
    }

    public long getDifficulty() {
        return difficulty;
    }

    public long getExercise() {
        return exercise;
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", description='" + description + '\'' +
                ", numberOfUse=" + numberOfUse +
                ", difficulty=" + difficulty +
                ", exercise=" + exercise +
                "}\n" + answers.toString();
    }

    @Override
    public Type getType() {
        return Type.Test;
    }

}
