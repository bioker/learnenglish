package net.bioker.android.learnenglish.model.draganddrop;

import android.graphics.Color;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.ListView;

import net.bioker.android.learnenglish.activities.LessonActivity;
import net.bioker.android.learnenglish.adapters.CompareAnswersAdapter;
import net.bioker.android.learnenglish.model.compare.Compare;
import net.bioker.android.learnenglish.model.compare.Pair;

import org.json.JSONException;

import java.util.ArrayList;

public class CompareAnswerOnDragListener implements View.OnDragListener {

    long srcPair;
    int dragColor;
    int resumeColor;
    int errorColor;

    public CompareAnswerOnDragListener(int dragColor, int resumeColor, int errorColor, long srcPair) {
        this.dragColor = dragColor;
        this.srcPair = srcPair;
        this.resumeColor = resumeColor;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int color = resumeColor;
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                Log.d("MYTAG", "Item ACTION_DRAG_STARTED: " + "\n");
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                Log.d("MYTAG", "Item ACTION_DRAG_ENTERED: " + "\n");
                v.setBackgroundColor(dragColor);
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                Log.d("MYTAG", "Item ACTION_DRAG_EXITED: " + "\n");
                v.setBackgroundColor(resumeColor);
                break;
            case DragEvent.ACTION_DROP:
                Log.d("MYTAG", "Item ACTION_DROP: " + "\n");

                CompareAnswerView compareAnswerView = (CompareAnswerView) event.getLocalState();
                long destPair = compareAnswerView.getPair();

                Log.d("MYTAG", "selected pair id - " + srcPair);
                Log.d("MYTAG", "destination pair id - " + destPair);

                if (srcPair == destPair) {
                    View view = compareAnswerView.getView();

                    ListView oldParent = (ListView) view.getParent();
                    CompareAnswersAdapter srcAdapter = (CompareAnswersAdapter) (oldParent.getAdapter());

                    ListView newParent = (ListView) v.getParent();
                    CompareAnswersAdapter destAdapter = (CompareAnswersAdapter) (newParent.getAdapter());
                    /*
                     * If drag and drop on the same list,
                     * ignore
                     */
                    if (srcAdapter != destAdapter) {
                        Log.d("MYTAG", "pairs matches and adapters not");

                        srcAdapter.removeItem(srcPair);
                        destAdapter.removeItem(destPair);

                        ArrayList<Pair> srcPairs = srcAdapter.getPairs();
                        ArrayList<Pair> destPairs = destAdapter.getPairs();

                        oldParent.setAdapter(new CompareAnswersAdapter(
                                        v.getContext(), srcPairs, srcAdapter.isLeft()));
                        newParent.setAdapter(new CompareAnswersAdapter(
                                v.getContext(), destPairs, destAdapter.isLeft()));

                        if((srcPairs.size()) < 2 && (destPairs.size() < 2)){
                            try {
                                LessonActivity.launchExercise(view.getContext());
                            } catch (JSONException e) {
                                Log.e("MYTAG", "can't launch exercise");
                            }
                        } else {
                            Log.d("MYTAG", "pairs more than 1");
                        }
                    } else {
                        Log.d("MYTAG", "adapters matches");
                    }
                } else {
                    Log.d("MYTAG", "pairs not matches");
                    color = errorColor;
                }
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                Log.d("MYTAG", "Item ACTION_DRAG_ENDED: " + "\n");
                v.setBackgroundColor(color);
            default:
                break;
        }

        return true;
    }

}