package net.bioker.android.learnenglish.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.db.DBOpenHelper;
import net.bioker.android.learnenglish.db.control.DMainController;
import net.bioker.android.learnenglish.loader.Loader;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class LoaderActivity extends AppCompatActivity {
    ProgressBar progress;
    TextView text;

    public static DBOpenHelper oh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oh =new DBOpenHelper(this.getApplicationContext());
        DMainController.database = new DBOpenHelper(getApplicationContext()).getWritableDatabase();
        oh.onUpgrade(DMainController.database,1,2);

        setContentView(R.layout.activity_load);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        text = (TextView) findViewById(R.id.textView3);

        if(avilableNetwork()){
            if(serverIsOnline()){
                update();
            }
            else ((TextView) findViewById(R.id.textView4)).setText("SERVER IS OFFLINE");
        }
        else ((TextView) findViewById(R.id.textView4)).setText("Not available network");

    }
    private void update(){
        Loader load = new Loader(progress,text,this);
        load.execute();
    }

    public boolean avilableNetwork() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = cm.getActiveNetworkInfo();
        if (nInfo != null && nInfo.isConnected()) {
            return true;
        }
        else {
            return false;
        }
    }
    public boolean serverIsOnline(){
        try{
            URL myUrl = new URL(Loader.CURRENT_URL);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(2000);
            connection.connect();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
