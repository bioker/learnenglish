package net.bioker.android.learnenglish.loader;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import net.bioker.android.learnenglish.db.control.DMainController;
import net.bioker.android.learnenglish.db.model.*;
import net.bioker.android.learnenglish.db.model.DMain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Importer {
    private static SQLiteDatabase base = DMainController.database;

    public static void loadJson(JSONObject obj,String table) throws JSONException {
        switch (table){
            case "theme":{loadTheme(obj);break;}
            case "exercise":{loadExercise(obj);break;}

            case "test":{loadTest(obj);break;}
            case "test_answer":{loadTest_answer(obj);break;}

            case "compare":{loadCompare(obj);break;}
            case "pair":{loadPair(obj);break;}

            case "simple":{loadEdit(obj);break;}
            case "simple_answer":{loadEdit_answer(obj);break;}

            case "version":{loadVersion(obj);break;}

            case "fix":{doFixes(obj);break;}
        }

    }
    private static boolean alreadyCreated(String table,int id){
        Cursor pointer;
        pointer = base.query(
                table,
                new String[]{"count(_ID)"},
                "_ID ="+id,
                null,
                null,
                null,
                null
        );
        pointer.moveToLast();
        Log.d("Importer", table+" ID="+id+String.valueOf(pointer.getInt(0)));
        return pointer.getInt(0)!=0;
    }


    private static int addExercise(int exercise, int type, int id) {
            ContentValues content = new ContentValues();
            content.put(DMain.EXERCISE_LESSON, exercise);
            content.put(DMain.EXERCISE_IDINTABLE, id);
            content.put(DMain.EXERCISE_TYPE, type);
            base.insert(DMain.EXERCISE, null, content);
            Log.d("Importer","exe-"+content.toString());
            content.clear();
            Cursor pointer;
            pointer = base.query(
                    DMain.EXERCISE,
                    new String[]{"_ID"},
                    null,
                    null,
                    null,
                    null,
                    "_ID"
            );
        pointer.moveToLast();
        Log.d("Importer","EXERCISE"+String.valueOf(pointer.getInt(0)));
        return pointer.getInt(0);
    }


//THEME
    private static void loadTheme(JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("theme");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);
            if(alreadyCreated(DMain.THEME,current.getInt("id")))
                base.delete(DMain.THEME,"_ID="+current.getInt("id"),null);
            ContentValues content = new ContentValues();
            content.put("_ID", current.getInt("id"));
            content.put(DMain.THEME_NAME, current.getString("name"));
            content.put(DMain.THEME_DESCRIPTION, current.getString("description"));
            base.insert(DMain.THEME, null, content);
            content.clear();
        }
    }
//EXERCISE=LESSON*
    private static void loadExercise(JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("exercise");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);
            if(alreadyCreated(DMain.LESSON,current.getInt("id"))){
                base.delete(DMain.LESSON,"_ID="+current.getInt("id"),null);
                Log.d("Loader", "Exercise deleted");
            }
            ContentValues content = new ContentValues();
            content.put("_ID", current.getInt("id"));
            content.put(DMain.LESSON_NAME, current.getString("name"));
            content.put(DMain.LESSON_DESCRIPTION, current.getString("description"));
            content.put(DMain.LESSON_THEME, current.getString("theme"));
            Log.d("Loader", String.valueOf(base.insert(DMain.LESSON, null, content)));
            content.clear();
        }
    }
//TESTpublic static void

    private static void loadTest(JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("test");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);
            if(alreadyCreated(DTest.TEST,current.getInt("id")))
                base.delete(DTest.TEST,"_ID="+current.getInt("id"),null);
            ContentValues content = new ContentValues();
            content.put("_ID", current.getInt("id"));
            content.put(DTest.TEST_QUESTION, current.getString("question"));
            content.put(DTest.TEST_DESCRIPTION, current.getString("description"));
            content.put(DTest.TEST_DIFFICULTY, current.getInt("difficult"));
            content.put(DTest.TEST_EXERCISE,
                    addExercise(
                        current.getInt("exercise"),
                        DMain.TYPE_TEST,
                        current.getInt("id")
                    )
            );
            base.insert(DTest.TEST, null, content);
            Log.d("Importer",content.toString());
            content.clear();

        }
    }

    private static void loadTest_answer(JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("test_answer");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);
            if(alreadyCreated(DTest.ANSWER,current.getInt("id")))
                base.delete(DTest.ANSWER,"_ID="+current.getInt("id"),null);
            ContentValues content = new ContentValues();
            content.put("_ID", current.getInt("id"));
            content.put(DTest.ANSWER_TEXT, current.getString("text"));
            content.put(DTest.ANSWER_DESCRIPTION, current.getString("error_description"));
            content.put(DTest.ANSWER_ISRIGHT, current.getInt("istrue"));
            content.put(DTest.ANSWER_TEST, current.getInt("test"));
            base.insert(DTest.ANSWER, null, content);
            content.clear();
        }
    }

    //COMPARE
    private static void loadCompare (JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("compare");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);

            if(alreadyCreated(DCompare.COMPARE,current.getInt("id")))
                base.delete(DCompare.COMPARE,"_ID="+current.getInt("id"),null);

                ContentValues content = new ContentValues();
                content.put("_ID", current.getInt("id"));
                content.put(DCompare.COMPARE_DESCRIPTION, current.getString("description"));
                content.put(DCompare.COMPARE_EXERCISE,
                    addExercise(
                        current.getInt("exercise"),
                        DMain.TYPE_COMPARE,
                        current.getInt("id")
                    )
                );
                content.put(DCompare.COMPARE_NAME, current.getString("name"));
                content.put(DCompare.COMPARE_DIFFICULT, current.getInt("difficult"));
                base.insert(DCompare.COMPARE, null, content);
                Log.d("Importer",content.toString());
                content.clear();
        }
    }

    private static void loadPair (JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("pair");

        for(int i=0; i<currentArray.length(); i++){

            JSONObject current = currentArray.getJSONObject(i);

            if(alreadyCreated(DCompare.PAIR,current.getInt("id")))
                base.delete(DCompare.PAIR,"_ID="+current.getInt("id"),null);

                ContentValues content = new ContentValues();
                content.put("_ID", current.getInt("id"));
                content.put(DCompare.PAIR_DESCRIPTION, current.getString("error"));
                content.put(DCompare.PAIR_ANSWER1, current.getString("answer1"));
                content.put(DCompare.PAIR_ANSWER2, current.getString("answer2"));
                content.put(DCompare.PAIR_COMPARE, current.getInt("compare"));
                base.insert(DCompare.PAIR, null, content);
                content.clear();

        }
    }
//EDIT
    private static void loadEdit(JSONObject obj) throws JSONException {
    JSONArray currentArray = obj.getJSONArray("simple");

    for(int i=0; i<currentArray.length(); i++){
        JSONObject current = currentArray.getJSONObject(i);
        if(alreadyCreated(DEdit.EDIT,current.getInt("id")))
            base.delete(DEdit.EDIT,"_ID="+current.getInt("id"),null);
        ContentValues content = new ContentValues();
        content.put("_ID", current.getInt("id"));
        content.put(DEdit.EDIT_QUESTION, current.getString("question"));
        content.put(DEdit.EDIT_DESCRIPTION, current.getString("description"));
        content.put(DEdit.EDIT_EXERCISE,
                addExercise(
                        current.getInt("exercise"),
                        DMain.TYPE_EDIT,
                        current.getInt("id")
                )
        );
        base.insert(DEdit.EDIT, null, content);
        Log.d("Importer",content.toString());
        content.clear();

    }
}

    private static void loadEdit_answer(JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("simple_answer");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);
            if(alreadyCreated(DEdit.ANSWER,current.getInt("id")))
                base.delete(DEdit.ANSWER,"_ID="+current.getInt("id"),null);
            ContentValues content = new ContentValues();
            content.put("_ID", current.getInt("id"));
            content.put(DEdit.ANSWER_TEXT, current.getString("text"));
            content.put(DEdit.ANSWER_DESCRIPTION, current.getString("error_description"));
            content.put(DEdit.ANSWER_TEST, current.getInt("simple"));
            base.insert(DEdit.ANSWER, null, content);
            Log.d("Importer",content.toString());
            content.clear();
        }
    }

    private static void loadVersion(JSONObject obj) throws JSONException  {
        JSONArray currentArray = obj.getJSONArray("version");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);
            if(alreadyCreated(DVersion.VERSION,current.getInt("id"))){
                base.delete(DVersion.VERSION,"_ID="+current.getInt("id"),null);
                Log.d("Loader", "Version deleted");
            }
            ContentValues content = new ContentValues();
            content.put("_ID", current.getInt("id"));
            content.put(DVersion.VERSION_DATETIME, current.getString("last_change"));
            content.put(DVersion.VERSION_DESCRIPTION, current.getString("description"));
            base.insert(DVersion.VERSION, null, content);
            Log.d("Loader", String.valueOf("Version add"+current.getInt("id")));
            content.clear();
        }
    }

    private static void doFixes(JSONObject obj) throws JSONException {
        JSONArray currentArray = obj.getJSONArray("fix");

        for(int i=0; i<currentArray.length(); i++){
            JSONObject current = currentArray.getJSONObject(i);
            String table = null;
            switch (current.getString("table")){
                case "theme":{table=DMain.THEME;break;}
                case "exercise":{table=DMain.LESSON;break;}
                case "test":{table=DTest.TEST;break;}
                case "answer":{table=DTest.ANSWER;break;}
                case "compare":{table=DCompare.COMPARE;break;}
                case "pair":{table=DCompare.PAIR;break;}
            }
            if(table == DMain.THEME||table == DMain.LESSON)
                base.delete(table,"_ID="+current.getString("intable_id"),null);
            else{
                base.delete(table,"_ID="+current.getString("intable_id"),null);
                base.delete(DMain.EXERCISE,DMain.EXERCISE_IDINTABLE+"="+current.getString("intable_id"),null);
            }
        }

    }


}



