package net.bioker.android.learnenglish.db.control;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.learnenglish.db.model.DEdit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Администратор on 01.08.2016.
 */
public class DEditController {
    static SQLiteDatabase database = DMainController.database;

    private static void plusParam(String table, String paramName, int id, int sum) {
        database.execSQL("UPDATE " + table + " set " + paramName + " = " + paramName + " + " + sum + " where _ID = " + id + ";");
    }

    private static JSONArray getAnswers(long testID) throws JSONException {
        Cursor pointer;
        pointer = database.query(
                DEdit.ANSWER,
                new String[]{"*"},
                DEdit.ANSWER + "." + DEdit.ANSWER_TEST + " = " + testID,
                null,
                null,
                null,
                null);

        if (pointer.moveToFirst()) {
            JSONArray jarr = new JSONArray();
            do {
                JSONObject jobj= new JSONObject();

                jobj.put("_ID",
                        pointer.getLong(pointer.getColumnIndex("_ID")));

                jobj.put(DEdit.ANSWER_TEST,
                        pointer.getLong(pointer.getColumnIndex(DEdit.ANSWER_TEST)));

                jobj.put(DEdit.ANSWER_TEXT,
                        pointer.getString(pointer.getColumnIndex(DEdit.ANSWER_TEXT)));

                jobj.put(DEdit.ANSWER_DESCRIPTION,
                        pointer.getString(pointer.getColumnIndex(DEdit.ANSWER_DESCRIPTION)));

                jarr.put(jobj);
            } while (pointer.moveToNext());
            return jarr;
        } else return null;
    }

    public static JSONArray get(long ID) throws JSONException {
        Cursor pointer;
        pointer = database.query(
                DEdit.EDIT,
                new String[]{"*"},
                DEdit.EDIT + "._ID = " + ID,
                null,
                null,
                null,
                null);
        JSONObject jobj= new JSONObject();
        if (pointer.moveToFirst()) {
            jobj.put("_ID",
                    pointer.getLong(pointer.getColumnIndex("_ID")));
            jobj.put(DEdit.EDIT_QUESTION,
                    pointer.getString(pointer.getColumnIndex(DEdit.EDIT_QUESTION)));
            jobj.put(DEdit.EDIT_DESCRIPTION,
                    pointer.getString(pointer.getColumnIndex(DEdit.EDIT_DESCRIPTION)));
            jobj.put(DEdit.EDIT_EXERCISE,
                    pointer.getLong(pointer.getColumnIndex(DEdit.EDIT_EXERCISE)));

            JSONArray jarr = new JSONArray();
            jarr.put(jobj);
            jarr.put(getAnswers(pointer.getLong(pointer.getColumnIndex("_ID"))));
            jarr.put(new JSONObject().put("TYPE","simple"));
            Log.d("JSON",jarr.toString());
            return jarr;
        } else {
            return null;
        }
    }
}
