package net.bioker.android.learnenglish.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import net.bioker.android.learnenglish.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void setToMain(View view) {
        Intent intent = new Intent(this, ThemeActivity.class);
        this.startActivity(intent);
    }

    public void setToLoad(View view) {
        Intent intent = new Intent(this, LoaderActivity.class);
        this.startActivity(intent);
    }

    public void setToProfile(View view){
        Intent intent = new Intent(this, ProfileActivity.class);
        this.startActivity(intent);
    }
}
