package net.bioker.android.learnenglish.loader;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.bioker.android.learnenglish.activities.ThemeActivity;
import net.bioker.android.learnenglish.db.control.DVersionController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Loader extends AsyncTask <Integer,Integer,String>{
    public static final String CURRENT_URL= "http://youdont.xyz/Task.php";
    public static final int CURRENT_VERSION = DVersionController.getCurrentVersion();
    public static final String TABLE_IMPORT[] = {
            "theme",
            "exercise",
            "compare","pair",
            "simple","simple_answer",
            "test","test_answer","pair","version","fix"
    };



    HttpURLConnection connection = null;
    Importer importer = new Importer();
    BufferedReader reader = null;
    String resultJson = "";
    int COUNTER =0;
    ProgressBar progress;
    TextView text;
    Context context;

    public Loader(ProgressBar progress, TextView text ,Context context) {
        this.progress=progress;
        this.text=text;
        this.context=context;
        progress.setMax(9);
    }

    private boolean load() throws MalformedURLException {
        return false;
    }

    @Override
    protected String doInBackground(Integer... params) {
        try {
            for(String table: TABLE_IMPORT) {
                String path = new String(CURRENT_URL+
                        "?table="+table+
                        "&version="+CURRENT_VERSION);
                Log.d("Loader", path);
                URL url = new URL(path);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;

                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                resultJson = buffer.toString();
                if(resultJson.length()>3){
                    publishProgress(++COUNTER);
                    JSONObject Allobjects= new JSONObject(resultJson);
                    Log.d("Loader", Allobjects.toString());

                    Importer.loadJson(Allobjects,table);
                }
                else{
                    Log.d("Loader", "Server empty string!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Loader", e.getMessage());
        }
        catch (JSONException e) {
            e.printStackTrace();
            Log.d("Loader", e.getMessage());
        }
        Log.d("Loader", "CURRENTVERSION="+DVersionController.getCurrentVersion());
        return null;
    }


    @Override
    protected void onProgressUpdate(Integer... items) {
        super.onProgressUpdate(items);
        progress.setProgress(items[0]);
    }

    @Override
    protected void onPostExecute(String content) {
        Intent intent = new Intent(context, ThemeActivity.class);
        context.startActivity(intent);
    }
}

