package net.bioker.android.learnenglish.model;

import java.util.ArrayList;

public class Lesson {

    private long id;
    private long theme;
    private String name;
    private ArrayList<Integer> exerciseIds;
    private String description;

    public long getId() {
        return id;
    }

    public long getTheme() {
        return theme;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Integer> getExerciseIds() {
        return exerciseIds;
    }

    public String getDescription() {
        return description;
    }

    public Lesson(long id, long theme, String name, ArrayList<Integer> exerciseIds, String description) {
        this.id = id;
        this.theme = theme;
        this.name = name;
        this.exerciseIds = exerciseIds;
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }

}
