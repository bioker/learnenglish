package net.bioker.android.learnenglish.db.model;

public class DTest {

    public static final String TEST = "TEST";
    public static final String TEST_QUESTION = "QUESTION";
    public static final String TEST_DESCRIPTION = "DESCRIPTION";
    public static final String TEST_EXERCISE = "EXERCISE";
    public static final String TEST_DIFFICULTY = "DIFFICULTY";
    public static final String TEST_USE = "NUMOFUSE";

    public static final String ANSWER = "TEST_ANSWER";
    public static final String ANSWER_TEXT = "ANSWER";
    public static final String ANSWER_DESCRIPTION = "ERROR_DESCRIPTION";
    public static final String ANSWER_ISRIGHT = "ISTRUE";
    public static final String ANSWER_TEST = "TEST";
    public static final String ANSWER_USE= "NUMOFUSE";

    public static final String TABLE_CREATE_TEST = "CREATE TABLE "+TEST
            +" (_ID INTEGER PRIMARY KEY, "
            +TEST_QUESTION+" TEXT NOT NULL, "
            +TEST_DESCRIPTION+" TEXT NULL, "
            +TEST_USE+" INTEGER NOT NULL DEFAULT 0, "
            +TEST_DIFFICULTY+" INTEGER NOT NULL, "
            + TEST_EXERCISE +" INTEGER, FOREIGN KEY("+ DMain.EXERCISE +") REFERENCES THEME(_ID));";

    public static final String TABLE_CREATE_ANSWERS = "CREATE TABLE "+ANSWER
            +" (_ID INTEGER PRIMARY KEY, "
            +ANSWER_TEXT+" TEXT NOT NULL, "
            +ANSWER_DESCRIPTION+" TEXT NULL, "
            +ANSWER_ISRIGHT+" INTEGER NOT NULL, "
            +ANSWER_USE+" INTEGER NOT NULL   DEFAULT 0, "
            + ANSWER_TEST +" INTEGER, FOREIGN KEY("+ ANSWER_TEST +") REFERENCES QUESTION(_ID));";

}
