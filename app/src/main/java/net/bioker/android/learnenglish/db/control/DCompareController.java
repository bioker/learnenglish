package net.bioker.android.learnenglish.db.control;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.bioker.android.learnenglish.db.model.DCompare;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DCompareController {

    static SQLiteDatabase database = DMainController.database;

    private static JSONArray getPairs(long ID) throws JSONException {
        Cursor pointer;
        pointer = database.query(
                DCompare.PAIR,
                new String[]{"*"},
                DCompare.PAIR+"."+ DCompare.PAIR_COMPARE+" = "+ID,
                null,
                null,
                null,
                null);

        if(pointer.moveToFirst()){
            JSONArray jarr = new JSONArray();
            do{
                JSONObject jobj= new JSONObject();
                jobj.put("_ID",pointer.getLong(pointer.getColumnIndex("_ID")));
                jobj.put(DCompare.PAIR_ANSWER1,pointer.getString(pointer.getColumnIndex(DCompare.PAIR_ANSWER1)));
                jobj.put(DCompare.PAIR_ANSWER2,pointer.getString(pointer.getColumnIndex(DCompare.PAIR_ANSWER2)));
                jobj.put(DCompare.COMPARE_DESCRIPTION,pointer.getString(pointer.getColumnIndex(DCompare.COMPARE_DESCRIPTION)));

                jarr.put(jobj);
            }while (pointer.moveToNext());
            return jarr;
        }
        else return null;
    }

    public static JSONArray get(long ID) throws JSONException {
        Cursor pointer;
        pointer = database.query(
                DCompare.COMPARE,
                new String[]{"*"},
                DCompare.COMPARE+"._ID = "+ID,
                null,
                null,
                null,
                null);

        if(pointer.moveToFirst()) {
            JSONArray jarr = new JSONArray();
            JSONObject jobj= new JSONObject();

            jobj.put("_ID",pointer.getLong(pointer.getColumnIndex("_ID")));
            jobj.put("PAIRS",getPairs(pointer.getLong(pointer.getColumnIndex("_ID"))));
            jobj.put(DCompare.COMPARE_EXERCISE,pointer.getLong(pointer.getColumnIndex(DCompare.COMPARE_EXERCISE)));
            jobj.put(DCompare.COMPARE_DESCRIPTION,pointer.getString(pointer.getColumnIndex(DCompare.COMPARE_DESCRIPTION)));

            jarr.put(jobj);
            jarr.put(new JSONObject().put("TYPE","compare"));
            return jarr;
        }
        else{
            return null;
        }
    }
}
