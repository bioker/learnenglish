package net.bioker.android.learnenglish.model.profile;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.db.DBOpenHelper;
import net.bioker.android.learnenglish.db.control.DMainController;
import net.bioker.android.learnenglish.db.control.DProfileController;
import net.bioker.android.learnenglish.model.Lesson;
import net.bioker.android.learnenglish.model.Theme;

import java.util.ArrayList;

public class ProgressInfoItem {

    private String type;
    private String name;
    private String value;

    public ProgressInfoItem(String type, String name, String value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "ProgressInfoItem{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public static ArrayList<ProgressInfoItem> getSummaryProfileInfo(Context context) {
        SQLiteDatabase db = new DBOpenHelper(context).getReadableDatabase();
        ArrayList<Theme> themes = DMainController.getAllThemes();
        if (themes == null) {
            Log.e("Profile", "themes is null!");
        }
        ArrayList<ProgressInfoItem> result = new ArrayList<>();
        for (Theme theme : themes) {

            String themeProgress =
                    String.valueOf(DProfileController.progressCoefForTheme(db, theme.getId()));
            String themeErrors =
                    String.valueOf(DProfileController.errorsCoefForTheme(db, theme.getId()));

            Log.d("Profile", "theme info:\n" + "theme: " + theme.getName() +
                    "\nprogress: " + themeProgress + "\nerrors: " + themeErrors);

            result.add(new ProgressInfoItem(context.getString(R.string.profileThemeProgressText),
                    theme.getName(),
                    themeProgress));
            result.addAll(getLessonsProgressInfo(context, theme.getId()));
            result.add(new ProgressInfoItem(context.getString(R.string.profileThemeErrorsText),
                    theme.getName(),
                    themeErrors));
            result.addAll(getLessonsErrorsInfo(context, theme.getId()));
        }
        return result;
    }

    public static ArrayList<ProgressInfoItem> getLessonsProgressInfo(Context context, long themeId) {
        SQLiteDatabase db = new DBOpenHelper(context).getReadableDatabase();
        ArrayList<Lesson> lessons = DMainController.getAllLessons(themeId);
        ArrayList<ProgressInfoItem> result = new ArrayList<>();
        for (Lesson lesson : lessons) {

            String progress =
                    String.valueOf(DProfileController.progressCoefForLesson(db, lesson.getId()));

            Log.d("Profile", "lesson info:\n" + "lesson: " + lesson.getName() +
                    "\nprogress: " + progress);

            result.add(new ProgressInfoItem(context.getString(R.string.profileLessonProgressText),
                    lesson.getName(),
                    progress));
        }
        return result;
    }

    public static ArrayList<ProgressInfoItem> getLessonsErrorsInfo(Context context, long themeId) {
        SQLiteDatabase db = new DBOpenHelper(context).getReadableDatabase();
        ArrayList<Lesson> lessons = DMainController.getAllLessons(themeId);
        ArrayList<ProgressInfoItem> result = new ArrayList<>();
        for (Lesson lesson : lessons) {

            String errors =
                    String.valueOf(DProfileController.errorsCoefForLesson(db, lesson.getId()));

            Log.d("Profile", "lesson info:\n" + "lesson: " + lesson.getName() +
                    "\nerrors: " + errors);

            result.add(new ProgressInfoItem(context.getString(R.string.profileLessonErrorsText),
                    lesson.getName(),
                    errors));
        }
        return result;
    }
}
