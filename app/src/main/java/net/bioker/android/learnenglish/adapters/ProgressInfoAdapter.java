package net.bioker.android.learnenglish.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.model.profile.ProgressInfoItem;

import java.util.ArrayList;

public class ProgressInfoAdapter extends BaseAdapter{

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<ProgressInfoItem> items;

    public ProgressInfoAdapter(Context context, ArrayList<ProgressInfoItem> items){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if(v == null){
            v = inflater.inflate(R.layout.progress_info_item, viewGroup, false);
        }

        ProgressInfoItem item = getProgressInfoItem(i);

        Log.d("Profile","find item:\n" + item);

        ((TextView)v.findViewById(R.id.profileItemTypeText)).setText(item.getType());
        ((TextView)v.findViewById(R.id.profileItemNameText)).setText(item.getName());
        ((TextView)v.findViewById(R.id.profileItemValueText)).setText(item.getValue());

        return v;
    }

    public ProgressInfoItem getProgressInfoItem(int i){
        return items.get(i);
    }
}
