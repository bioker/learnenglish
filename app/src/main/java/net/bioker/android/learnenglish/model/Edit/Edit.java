package net.bioker.android.learnenglish.model.Edit;

import android.util.Log;
import android.util.Pair;

import net.bioker.android.learnenglish.db.model.DEdit;
import net.bioker.android.learnenglish.db.model.DTest;
import net.bioker.android.learnenglish.model.Exercise;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Администратор on 01.08.2016.
 */
public class Edit extends Exercise {

    @Override
    public String getDescription() {
        return null;
    }
    public Edit(String JSONString) throws JSONException {
        super(Type.Edit);
        JSONArray JSON = new JSONArray(JSONString);Log.d("JSON",JSON.toString());
        JSONArray JSONa = JSON.getJSONArray(1);
        Log.d("JSON","TEST-"+JSON.toString());
        Log.d("JSON","A-"+JSONa.toString());
        ArrayList<Pair<String,String>> answers = new ArrayList<>();
        for(int i=0; i<JSONa.length(); i++){
            JSONObject JSONPair = JSONa.getJSONObject(i);
            answers.add(
                    new Pair<String, String>(
                            JSONPair.getString(DEdit.ANSWER_TEXT),
                            JSONPair.getString(DEdit.ANSWER_DESCRIPTION)
                    )
            );
        }

        this.id = JSON.getJSONObject(0).getInt("_ID");
        this.question = JSON.getJSONObject(0).getString(DEdit.EDIT_QUESTION);
        this.description = JSON.getJSONObject(0).getString(DEdit.EDIT_DESCRIPTION);
        this.exercise = JSON.getJSONObject(0).getInt(DEdit.EDIT_EXERCISE);
        this.answers = answers;

    }

    public ArrayList<Pair<String,String>> answers;
    private long id;
    private String question;
    private String description;
    private long numberOfUse;
    private long exercise;

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", description='" + description + '\'' +
                ", numberOfUse=" + numberOfUse +
                ", exercise=" + exercise +
                "}\n" + answers.toString();
    }

    public String getQuestion() {
        return question;
    }

    public ArrayList<Pair<String,String>> getAnswers() {
        return answers;
    }
}
