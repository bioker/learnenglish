package net.bioker.android.learnenglish.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.bioker.android.learnenglish.R;
import net.bioker.android.learnenglish.activities.LessonActivity;

import org.json.JSONException;

/**
 * Created by Администратор on 03.08.2016.
 */
public class NextExercise extends DialogFragment {
    Context context;
    String error;
    Boolean isright;
    public NextExercise(Activity current){
        super();
        context = current;
    }

    public void wrongAns(String error,Boolean isright){
        this.isright=isright;
        this.error=error;
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.nextexercisedialog, container, false);

        TextView verror = (TextView) rootView.findViewById(R.id.errordescriprion);
        TextView vresult = (TextView) rootView.findViewById(R.id.result);
                if(isright){
                    vresult.setText("Правильно!");
                    verror.setHeight(0);
                }else{
                    vresult.setText("Не правильно!");
                    verror.setText(error);
                }
        Button dismiss = (Button) rootView.findViewById(R.id.cancel);

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button next = (Button) rootView.findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LessonActivity.launchExercise(rootView.getContext());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        return rootView;
    }

}
