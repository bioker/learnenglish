package net.bioker.android.learnenglish.model;

public class Theme {

    private long id;
    private String name;
    private String description;

    public Theme(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return name;
    }
}
