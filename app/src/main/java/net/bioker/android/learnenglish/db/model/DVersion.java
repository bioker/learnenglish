package net.bioker.android.learnenglish.db.model;

/**
 * Created by Администратор on 05.08.2016.
 */
public class DVersion {
    public static final String VERSION = "VERSION";
    public static final String VERSION_DATETIME = "DATETIME";
    public static final String VERSION_DESCRIPTION = "DESCRIPTION";

    public static final String TABLE_CREATE_VERSION = "CREATE TABLE "+ VERSION
            +" (_ID INTEGER PRIMARY KEY, "
            + VERSION_DATETIME +" TEXT NULL, "
            + VERSION_DESCRIPTION +" TEXT NULL); ";

}
