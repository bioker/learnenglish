package net.bioker.android.learnenglish.db.control;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.learnenglish.db.model.DTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DTestController {
    static SQLiteDatabase database = DMainController.database;

    private static void plusParam(String table, String paramName, int id, int sum) {
        database.execSQL("UPDATE " + table + " set " + paramName + " = " + paramName + " + " + sum + " where _ID = " + id + ";");
    }

    private static JSONArray getAnswers(long testID, long count) throws JSONException {
        JSONArray jarr = new JSONArray();
        Log.d("JSON","+Right");
        jarr.put(getRightAnswers(testID));
        Log.d("JSON",jarr.toString());

        Log.d("JSON","+Wrong");
        jarr.put(getWrongAnswers(testID, count));
        Log.d("JSON",jarr.toString());
        return jarr;
    }

    private static JSONArray getRightAnswers(long testID) throws JSONException {
        Cursor pointer;
        pointer = database.query(
                DTest.ANSWER,
                new String[]{"*"},
                DTest.ANSWER + "." + DTest.ANSWER_TEST + " = " + testID
                        + " AND " + DTest.ANSWER + "." + DTest.ANSWER_ISRIGHT + " = 1",
                null,
                null,
                null,
                DTest.ANSWER_USE);

        if (pointer.moveToFirst()) {
            JSONArray jarr = new JSONArray();
            do {
                JSONObject jobj= new JSONObject();

                jobj.put("_ID",
                        pointer.getLong(pointer.getColumnIndex("_ID")));

                jobj.put(DTest.ANSWER_TEST,
                        pointer.getLong(pointer.getColumnIndex(DTest.ANSWER_TEST)));

                jobj.put(DTest.ANSWER_TEXT,
                        pointer.getString(pointer.getColumnIndex(DTest.ANSWER_TEXT)));

                jobj.put(DTest.ANSWER_DESCRIPTION,
                        pointer.getString(pointer.getColumnIndex(DTest.ANSWER_DESCRIPTION)));

                jobj.put(DTest.ANSWER_ISRIGHT,
                        pointer.getInt(pointer.getColumnIndex(DTest.ANSWER_ISRIGHT))!=0);

                jobj.put(DTest.ANSWER_USE,
                        pointer.getLong(pointer.getColumnIndex(DTest.ANSWER_USE)));

                jarr.put(jobj);
            } while (pointer.moveToNext());
            return jarr;
        } else return null;
    }

    private static JSONArray getWrongAnswers(long testID, long count) throws JSONException {
        Cursor pointer;
        pointer = database.query(
                DTest.ANSWER,
                new String[]{"*"},
                DTest.ANSWER + "." + DTest.ANSWER_TEST + " = " + testID
                        + " AND " + DTest.ANSWER + "." + DTest.ANSWER_ISRIGHT + " = 0",
                null,
                null,
                null,
                DTest.ANSWER_USE);

        if (pointer.moveToFirst()) {
            JSONArray jarr = new JSONArray();
            for(long i = 0; count>i ;i++){
                JSONObject jobj= new JSONObject();

                jobj.put("_ID",
                        pointer.getLong(pointer.getColumnIndex("_ID")));
                jobj.put(DTest.ANSWER_TEST,
                        pointer.getLong(pointer.getColumnIndex(DTest.ANSWER_TEST)));
                jobj.put(DTest.ANSWER_TEXT,
                        pointer.getString(pointer.getColumnIndex(DTest.ANSWER_TEXT)));
                jobj.put(DTest.ANSWER_DESCRIPTION,
                        pointer.getString(pointer.getColumnIndex(DTest.ANSWER_DESCRIPTION)));
                jobj.put(DTest.ANSWER_ISRIGHT,
                        pointer.getInt(pointer.getColumnIndex(DTest.ANSWER_ISRIGHT))!= 0);
                jobj.put(DTest.ANSWER_USE,
                        pointer.getLong(pointer.getColumnIndex(DTest.ANSWER_USE)));

                jarr.put(jobj);
                plusParam(DTest.ANSWER, DTest.ANSWER_USE, pointer.getInt(pointer.getColumnIndex("_ID")), 1);
                if(!pointer.moveToNext()){break;}
                }
            return jarr;
        } else return null;
    }

    public static JSONArray get(long ID) throws JSONException {
        Cursor pointer;
        pointer = database.query(
                DTest.TEST,
                new String[]{"*"},
                DTest.TEST + "._ID = " + ID,
                null,
                null,
                null,
                null);
        JSONObject jobj= new JSONObject();
        if (pointer.moveToFirst()) {
                jobj.put("_ID",
                        pointer.getLong(pointer.getColumnIndex("_ID")));
                jobj.put(DTest.TEST_QUESTION,
                        pointer.getString(pointer.getColumnIndex(DTest.TEST_QUESTION)));
                jobj.put(DTest.TEST_DIFFICULTY,
                        pointer.getLong(pointer.getColumnIndex(DTest.TEST_DIFFICULTY)));
                jobj.put(DTest.TEST_DESCRIPTION,
                        pointer.getString(pointer.getColumnIndex(DTest.TEST_DESCRIPTION)));
                jobj.put(DTest.TEST_USE,
                        pointer.getLong(pointer.getColumnIndex(DTest.TEST_USE)));
                jobj.put(DTest.TEST_EXERCISE,
                        pointer.getLong(pointer.getColumnIndex(DTest.TEST_EXERCISE)));

                JSONArray jarr = new JSONArray();
                jarr.put(jobj);
                jarr.put(getAnswers(pointer.getLong(pointer.getColumnIndex("_ID")),
                        pointer.getLong(pointer.getColumnIndex(DTest.TEST_DIFFICULTY))));
            jarr.put(new JSONObject().put("TYPE","test"));
            Log.d("JSON",jarr.toString());
            return jarr;
        } else {
            return null;
        }
    }

}
