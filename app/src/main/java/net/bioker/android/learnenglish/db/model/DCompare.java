package net.bioker.android.learnenglish.db.model;

public class DCompare {

    public static final String COMPARE = "COMPARE";
    public static final String COMPARE_DIFFICULT = "DIFFICULT";
    public static final String COMPARE_NAME = "NAME";
    public static final String COMPARE_EXERCISE = "EXERCISE";
    public static final String COMPARE_DESCRIPTION = "DESCRIPTION";

    public static final String PAIR = "PAIR";
    public static final String PAIR_ANSWER1 = "ANSWER1";
    public static final String PAIR_ANSWER2 = "ANSWER2";
    public static final String PAIR_COMPARE = "COMPARE";
    public static final String PAIR_DESCRIPTION = "DESCRIPTION";

    public static final String TABLE_CREATE_SOPS = "CREATE TABLE "+ COMPARE
            +" (_ID INTEGER PRIMARY KEY, "
            + COMPARE_EXERCISE +" INTEGER,"
            + COMPARE_DESCRIPTION +" TEXT  NULL, "
            + COMPARE_DIFFICULT +" INTEGER  NULL, "
            + COMPARE_NAME +" TEXT  NULL, "
            +" FOREIGN KEY("+ COMPARE_EXERCISE +") REFERENCES "+DMain.EXERCISE +"(_ID)); ";

    public static final String TABLE_CREATE_PAIR = "CREATE TABLE "+PAIR
            +" (_ID INTEGER PRIMARY KEY, "
            + PAIR_COMPARE+" INTEGER,"
            +PAIR_ANSWER1+" TEXT NOT NULL, "
            +PAIR_ANSWER2+" TEXT NOT NULL, "
            +PAIR_DESCRIPTION +" TEXT  NULL, "
            +" FOREIGN KEY("+ PAIR_COMPARE +") REFERENCES "+ COMPARE +"(_ID)); ";
}
