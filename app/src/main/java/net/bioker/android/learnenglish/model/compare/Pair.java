package net.bioker.android.learnenglish.model.compare;

import android.os.Parcel;
import android.os.Parcelable;

public class Pair implements Parcelable {

    public static final Creator<Pair> CREATOR = new Creator<Pair>() {
        @Override
        public Pair createFromParcel(Parcel in) {
            return new Pair(in);
        }

        @Override
        public Pair[] newArray(int size) {
            return new Pair[size];
        }
    };

    private long id;
    private String answer1;
    private String answer2;
    private String description;

    public Pair(long id, String answer1, String answer2, String description) {
        this.id = id;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.description = description;
    }

    protected Pair(Parcel in) {
        id = in.readLong();
        answer1 = in.readString();
        answer2 = in.readString();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(answer1);
        dest.writeString(answer2);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getId() {
        return id;
    }

    public String getAnswer1() {
        return answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "id=" + id +
                ", answer1='" + answer1 + '\'' +
                ", answer2=" + answer2 +
                ", description='" + description + '\'';

    }
}
