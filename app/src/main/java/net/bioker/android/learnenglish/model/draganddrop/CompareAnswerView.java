package net.bioker.android.learnenglish.model.draganddrop;

import android.view.View;

public class CompareAnswerView {

    private View view;
    private long pair;

    public CompareAnswerView(long pair, View view) {
        this.pair = pair;
        this.view = view;
    }

    public long getPair() {
        return pair;
    }

    public View getView() {
        return view;
    }
}
