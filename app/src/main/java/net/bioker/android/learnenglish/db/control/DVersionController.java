package net.bioker.android.learnenglish.db.control;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.bioker.android.learnenglish.db.model.DVersion;

/**
 * Created by Администратор on 05.08.2016.
 */
public class DVersionController {
    public static SQLiteDatabase database = DMainController.database;
    public static int getCurrentVersion(){
         Cursor pointer;
         pointer = database.query(
                 DVersion.VERSION,
                 new String[]{"_ID"},
                 null,
                 null,
                 null,
                 null,
                 "_ID  DESC");
         if (pointer.moveToFirst()) {
             return pointer.getInt(pointer.getColumnIndex("_ID"));
         }
         else
             return -1;
     }
}

