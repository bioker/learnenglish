package net.bioker.android.learnenglish.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.bioker.android.learnenglish.model.compare.Compare;
import net.bioker.android.learnenglish.model.compare.Pair;
import net.bioker.android.learnenglish.model.draganddrop.CompareAnswerOnDragListener;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;

public class CompareAnswersAdapter extends BaseAdapter {

    private ArrayList<Pair> pairs;
    private Context context;
    private boolean isLeft;

    public CompareAnswersAdapter(Context context, ArrayList<Pair> pairs, boolean isLeft) {
        this.context = context;
        this.pairs = pairs;
        this.isLeft = isLeft;
    }

    @Override
    public int getCount() {
        return pairs.size();
    }

    @Override
    public Object getItem(int position) {
        Log.d("Compare", "getItem id - " + pairs.get(position).getId());
        return pairs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        Log.d("Compare", "getView:\nposition - " + position);
        // reuse views
        if (rowView == null) {
            Log.d("Compare", "rowView is null");
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            rowView = inflater.inflate(android.R.layout.simple_list_item_1, null);
            if (isLeft) {
                Log.d("Compare", "is left column value - " +
                        pairs.get(position).getAnswer1());
                ((TextView) rowView).setText(pairs.get(position).getAnswer1());
            } else {
                Log.d("Compare", "is right column value - " +
                        pairs.get(position).getAnswer2());
                ((TextView) rowView).setText(pairs.get(position).getAnswer2());
            }
        } else {
            Log.d("Compare", "rowView not null");
            Log.d("Compare", "text:\n" + ((TextView) rowView).getText());
        }

        rowView.setOnDragListener(
                new CompareAnswerOnDragListener(Color.GRAY, Color.WHITE, Color.RED,
                    pairs.get(position).getId()));

        return rowView;
    }

    public int getIndexOf(long pairId) {
        Log.d("Compare", "needPair - " + pairId);
        for (Pair pair : pairs) {
            Log.d("Compare", "havePair - " + pair.getId());
            if (pair.getId() == pairId) {
                Log.d("Compare", "index for removing - " + pairs.indexOf(pair));
                return pairs.indexOf(pair);
            }
        }
        throw new InvalidParameterException("pair with id " + pairId + "not found");
    }

    public ArrayList<Pair> getPairs() {
        return pairs;
    }

    public boolean removeItem(long pairId) {
        return pairs.remove(getItem(getIndexOf(pairId)));
    }

    public boolean isLeft() {
        return isLeft;
    }
}