package net.bioker.android.learnenglish.model;

public abstract class Exercise {

    public enum Type {
        Test(1), Compare(2), Edit(3), Audio(4);

        private int value;

        Type(Integer num) {
            value = num;
        }

        public int getValue() {
            return value;
        }
    }

    protected Exercise(Type type) {
        this.type = type;
    }

    protected Type type;

    public abstract String getDescription();

    public Type getType() {
        return type;
    }
}
